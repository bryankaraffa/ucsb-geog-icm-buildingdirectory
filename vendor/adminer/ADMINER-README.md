Adminer (formerly phpMinAdmin) is a full-featured database management tool written in PHP. Conversely to phpMyAdmin, it consist of a single file ready to deploy to the target server. Adminer is available for MySQL, PostgreSQL, SQLite, MS SQL, Oracle, SimpleDB, Elasticsearch and MongoDB.

To download the latest version of Adminer, you can use this command (in this Adminer directory)

     wget -O ./adminer.php http://www.adminer.org/latest-mysql-en.php
