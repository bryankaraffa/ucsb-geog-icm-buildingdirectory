<?php
include("../../config.php");
function adminer_object() {
    
    class AdminerSoftware extends Adminer {
        
        function name() {
            // custom name in title and heading
            return 'Building Directory Editor';
        }
        
        function permanentLogin() {
            // key used for permanent login
			// it is recommended that you change this for all new deployments
            return "8dfm37vm238xdmnm273jdfgdf820m3bd6765271283ncd5c6w873b4";
        }
        
        function credentials() {
			global $mysqlconfig;
            // server, username and password for connecting to database
            return array($mysqlconfig['host'], $mysqlconfig['username'], $mysqlconfig['password']);
        }
        
        function database() {
			global $mysqlconfig;
            // database name, will be escaped by Adminer
			return $mysqlconfig['dbname'];
        }
        
        function login($login, $password) {
			global $editor;
            // validate user submitted credentials
            return ($login == $editor['username'] && $password == $editor['password']);
        }
        
        //function tableName($tableStatus) {
          // tables without comments would return empty string and will be ignored by Adminer
	    //return h($tableStatus["Name"]);
        //}
        
//        function fieldName($field, $order = 0) {
//            // only columns with comments will be displayed and only the first five in select
//            return ($order <= 5 && !preg_match('~_(md5|sha1)$~', $field["field"]) ? h($field["comment"]) : "");
//      }
        
    }
    
    return new AdminerSoftware;
}
include "./adminer-editor.php";