var debugMode = 1;

function debugLog(message) {
  if (debugMode === 1) {
    console.log ('[DEBUG]: '+message);
  }
}

function clock() {
   var now = new Date();
   var outStr = now.getHours()+':'+(now.getMinutes()<10?'0':'')+now.getMinutes()+':'+(now.getSeconds()<10?'0':'')+now.getSeconds()+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+now.toDateString();
   document.getElementById('clockDiv').innerHTML=outStr;
   setTimeout('clock()',1000);
}

var currentFloorMap = 1;
function showFloorMap(floor_number) {
	debugLog('Enabling Floor '+floor_number);
	//$( "#img_floormap" ).attr("src","/floorplans/563-"+floor_number+"-1.png");
	$( "#div_floormap_"+currentFloorMap ).removeClass('show').addClass('hide');
	$( "#btn_floormap_"+currentFloorMap ).removeClass('btn-inverse').addClass('btn-primary');
	
	$( "#div_floormap_"+floor_number ).removeClass('hide').addClass('show');
	$( "#btn_floormap_"+floor_number ).removeClass('btn-primary').addClass('btn-inverse');
	
	
	currentFloorMap = floor_number;
}

$( document ).ready (function() {
  debugLog('jQuery loaded and $(document).ready() triggered.');
  
  // Initialize Clock
  clock();
  
  // Initialize the people buttons
  $( "#button_firsthalf" ).click(function() {
	$( "#div_firsthalf" ).removeClass('hide').addClass('show');
	$( "#div_secondhalf" ).removeClass('show').addClass('hide');
	$( "#div_facilities" ).removeClass('show').addClass('hide');
  });
  $( "#button_secondhalf" ).click(function() {
	$( "#div_firsthalf" ).removeClass('show').addClass('hide');
	$( "#div_secondhalf" ).removeClass('hide').addClass('show');
	$( "#div_facilities" ).removeClass('show').addClass('hide');
  });
  $( "#button_facilities" ).click(function() {
	$( "#div_secondhalf" ).removeClass('show').addClass('hide');
	$( "#div_firsthalf" ).removeClass('show').addClass('hide');
	$( "#div_facilities" ).removeClass('hide').addClass('show');
  });
  
  // Initialize the floormap buttons
  $( "#btn_floormap_1" ).click(function() {  showFloorMap(1)  });
  $( "#btn_floormap_2" ).click(function() {  showFloorMap(2)  });
  $( "#btn_floormap_3" ).click(function() {  showFloorMap(3)  });
  $( "#btn_floormap_4" ).click(function() {  showFloorMap(4)  });
  $( "#btn_floormap_5" ).click(function() {  showFloorMap(5)  });
  $( "#btn_floormap_6" ).click(function() {  showFloorMap(6)  });
});

debugLog('main.js Loaded');
