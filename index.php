<?php
// FatFree framework
$f3 = require ("vendor/fatfree/lib/base.php");

if (file_exists('config.php') && $_SERVER['REQUEST_URI'] != '/installer') {
	include('config.php');
	if(!isset($mysqlconfig)) {
	  die('MySQL config does not exists. Check config.php');
	}
	else {
		$db=new DB\SQL(
			'mysql:host='.$mysqlconfig['host'].';port='.$mysqlconfig['port'].';dbname='.$mysqlconfig['dbname'].'',
			$mysqlconfig['username'],  // MySQL Username for FatFreeFramework
			$mysqlconfig['password']   // MySQL Password for FatFreeFramework
		);
	}
}
elseif ($_SERVER['REQUEST_URI'] != '/installer') {
	header('Location: /installer');
}

// Set to 3 when in development mode, otherwise set to 0
$f3->set('DEBUG', 1);

$f3->route('GET /',
    function($f3) {
	$f3->set('page_title','Main Menu');
	$f3->set('building_name','Ellison Hall');
	
	$view=new Template;
	echo $view->render('templates/header.html');
	echo $view->render('templates/homepage.html');
	echo $view->render('templates/footer.html');
    }
);
$f3->route('GET /floormaps',
    function($f3) {
	$f3->set('page_title','Main Menu >> Floor Maps');
	$f3->set('building_name','Ellison Hall');
	
	$view=new Template;
	echo $view->render('templates/header.html');
	echo $view->render('templates/floormaps.html');
	echo $view->render('templates/button_mainmenu.html');
	echo $view->render('templates/idle-timer-js.html');
	echo $view->render('templates/footer.html');
    }
);
$f3->route('GET /directory',
    function($f3) {
      $f3->set('page_title','Main Menu >> People / Facilities Directory');
	  $f3->set('building_name','Ellison Hall');
	  
	  global $db;
	  $result=$db->exec('SELECT firstname,lastname,bldg,roomNumber,affiliation FROM people ORDER BY lastname ASC');
	  
	    
	  $firsthalf = array_slice($result, 0, count($result) / 2);
	  $f3->set('firsthalf',$firsthalf);
	  $f3->set('firsthalf_firstperson', array_shift($firsthalf)['lastname']);
	  $f3->set('firsthalf_lastperson', array_pop($firsthalf)['lastname']);
	  
	  $secondhalf = array_slice($result, count($result) / 2);
	  $f3->set('secondhalf',$secondhalf);
	  $f3->set('secondhalf_firstperson', array_shift($secondhalf)['lastname']);
	  $f3->set('secondhalf_lastperson', array_pop($secondhalf)['lastname']);
	  
	  
	  $result=$db->exec('SELECT name,category,bldg,roomNumber,affiliation FROM facilities ORDER BY name ASC');
	  $facilities=[];
	  foreach($result as $facility) {
		if (!isset($facilities[$facility['affiliation']][$facility['category']])) { $facilities[$facility['affiliation']][$facility['category']]=[]; }
		array_push($facilities[$facility['affiliation']][$facility['category']],$facility);
	  }
	  $f3->set('results',$facilities);
	    
	  
      $view=new Template;
      echo $view->render('templates/header.html');
      echo $view->render('templates/directory.html');
      echo $view->render('templates/button_mainmenu.html');
	  echo $view->render('templates/idle-timer-js.html');
      echo $view->render('templates/footer.html');
    }
);
$f3->route('GET /room-map/@bldg/@room',
    function($f3, $params) {
	$f3->set('page_title','Main Menu >> Map');
	$f3->set('building_name','Ellison Hall');
	$f3->set('bldg',$params['bldg']);
	$f3->set('room',$params['room']);
	
	$view=new Template;
	echo $view->render('templates/header.html');
	echo $view->render('templates/map.html');
	echo $view->render('templates/button_goback.html');
	echo $view->render('templates/button_mainmenu.html');
	echo $view->render('templates/idle-timer-js.html');
	echo $view->render('templates/footer.html');
    }
);
$f3->route('GET /installer',
	function($f3) {
	if (file_exists('config.php')) { echo('config.php already exists.  The installer will not run until this is deleted.'); die(); }
	$f3->set('page_title','Installer');
	$f3->set('building_name','');
	$view=new Template;
	echo $view->render('templates/header.html');
	echo $view->render('templates/installer.html');
	echo $view->render('templates/footer.html');	
	}
);
$f3->route('POST /installer',
	function($f3) {
	if (file_exists('config.php')) { echo('config.php already exists.  The installer will not run until this is deleted.'); die(); }
	$POST=$f3->get('POST');
	$mysqlhost='localhost';
	$mysqlport='3306';
	//echo json_encode($POST)."\n";
	
	$config=file_get_contents('config.php.sample');
	
	$config = str_replace("_your_username_", $POST['mysqlusername'], $config);
	$config = str_replace("_your_password_", $POST['mysqlpassword'], $config);
	$config = str_replace("_your_database_name_", $POST['dbname'], $config);
	$config = str_replace("editor_username", $POST['editorusername'], $config);
	$config = str_replace("editor_password", $POST['editorpassword'], $config);
	if ($POST['host'] != 'localhost' && $POST['host'] != '') {
		$config = str_replace("localhost", $POST['host'], $config);
		$mysqlhost = $POST['host'];
	}
	if ($POST['port'] != '3306' && $POST['port'] != '') {
		$config = str_replace("3306", $POST['port'], $config);
		$mysqlport = $POST['port'];
	}	
	
	$link = @mysqli_connect($mysqlhost, $POST['mysqlusername'], $POST['mysqlpassword'], $POST['dbname']);
	if(!$link) {
		die('Failed to connect to the server: ' . mysqli_connect_error());
	}

	if(!@mysqli_select_db($link, $POST['dbname'])) {
		die('Failed to connect to the database: ' . mysqli_error($link));
	}

	
	// Now that we've confirmed the MySQL credentials and DB name are valid... lets try to execute some SQL.
	$db=new DB\SQL(
		'mysql:host='.$mysqlhost.';port='.$mysqlport.';dbname='.$POST['dbname'].'',
		$POST['mysqlusername'],
		$POST['mysqlpassword'] 
	);
	$result=$db->exec(file_get_contents('sql/buildingdirectory.sql'));
	
	if ($result >= 0) {
		// Save the config once everything is has succeeded.
		file_put_contents('config.php',$config);
		if (file_get_contents('config.php') != $config) {
			die('Not able to update/write to config.php');
		}
	  }
	$f3->reroute("/installer-success");	
	}
);
$f3->route('GET /installer-success',
	function($f3) {
	$f3->set('page_title','Installer');
	$f3->set('building_name','');
	$view=new Template;
	echo $view->render('templates/header.html');
	echo $view->render('templates/installer-success.html');
	echo $view->render('templates/footer.html');	
	}
);
$f3->route('GET /editor',
	function($f3) {
	$f3->reroute("vendor/adminer/editor.php");	
	}
);
$f3->route('GET /adminer',
	function($f3) {
	$f3->reroute("vendor/adminer/adminer.php");	
	}
);

$f3->run();
?>
