SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `affiliations`;
CREATE TABLE `affiliations` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `abbreviation` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uid` (`uid`),
  KEY `acronym` (`abbreviation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `affiliations` (`uid`, `name`, `abbreviation`) VALUES
(1,	'Department of Geography',	'GEOG'),
(2,	'UCSB Interactive Campus Map Project',	'ICM');

DROP TABLE IF EXISTS `buildings`;
CREATE TABLE `buildings` (
  `bldgNumber` int(11) NOT NULL,
  `bldgName` varchar(255) NOT NULL,
  `bldgAbbreviation` varchar(20) NOT NULL,
  PRIMARY KEY (`bldgNumber`),
  UNIQUE KEY `bldgNumber` (`bldgNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `buildings` (`bldgNumber`, `bldgName`, `bldgAbbreviation`) VALUES
(563,	'Ellison Hall',	'ELLSN');

DROP TABLE IF EXISTS `facilities`;
CREATE TABLE `facilities` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `bldg` int(11) NOT NULL,
  `roomNumber` varchar(10) NOT NULL,
  `affiliation` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uid` (`uid`),
  KEY `category` (`category`),
  KEY `bldg` (`bldg`),
  KEY `affiliation` (`affiliation`),
  CONSTRAINT `facilities_ibfk_2` FOREIGN KEY (`category`) REFERENCES `facility_categories` (`name`),
  CONSTRAINT `facilities_ibfk_3` FOREIGN KEY (`bldg`) REFERENCES `buildings` (`bldgNumber`),
  CONSTRAINT `facilities_ibfk_4` FOREIGN KEY (`affiliation`) REFERENCES `affiliations` (`abbreviation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `facilities` (`uid`, `name`, `category`, `bldg`, `roomNumber`, `affiliation`) VALUES
(1,	'Star Lab',	'Instructional Labs',	563,	'2609',	'GEOG'),
(2,	'UCSB Interactive Campus Map Office',	'Instructional Labs',	563,	'1710',	'GEOG');

DROP TABLE IF EXISTS `facility_categories`;
CREATE TABLE `facility_categories` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uid` (`uid`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `facility_categories` (`uid`, `name`) VALUES
(1,	'Administration'),
(3,	'Conference Rooms'),
(4,	'Instructional Labs'),
(2,	'Research Labs / Groups');

DROP TABLE IF EXISTS `people`;
CREATE TABLE `people` (
  `uid` tinyint(4) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `bldg` int(11) NOT NULL,
  `roomNumber` varchar(10) NOT NULL,
  `affiliation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uid` (`uid`),
  KEY `bldg` (`bldg`),
  KEY `affiliation` (`affiliation`),
  CONSTRAINT `people_ibfk_1` FOREIGN KEY (`bldg`) REFERENCES `buildings` (`bldgNumber`),
  CONSTRAINT `people_ibfk_2` FOREIGN KEY (`affiliation`) REFERENCES `affiliations` (`abbreviation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `people` (`uid`, `firstname`, `lastname`, `bldg`, `roomNumber`, `affiliation`) VALUES
(80,	'Bryan',	'Karaffa',	563,	'1709',	'GEOG'),
(81,	'Sally',	'Seashell',	563,	'1707',	'GEOG'),
(82,	'Johny',	'Appleseed',	563,	'4816',	'GEOG'),
(83,	'Alex',	'Zoltan',	563,	'6813',	'GEOG');