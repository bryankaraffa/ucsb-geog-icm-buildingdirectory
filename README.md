# **Ellison Hall Building Directory**
## A sample application to demonstrate the capabilities of the [UCSB Interactive Campus Map](http://map.geog.ucsb.edu/)
#### Developed by the [UCSB Department of Geography](http://geog.ucsb.edu)

## What is the Ellison Hall Building Directory?

This is a simple PHP+HTML5 webapp that leverages the UCSB Interactive Campus Map's [public API](http://map.geog.ucsb.edu/api) "Quick Map" feature to display the locations on a map.  It is designed to run in a full-screen browser (Chrome's Kiosk Mode is suggested) on touchscreen monitors.  This application is also serves as an example to the extensibility of the UCSB Interactive Campus Map using only the [public API](http://map.geog.ucsb.edu/api).

For more information about the UCSB Interactive Campus Map project, you can visit the master repository [`ucsb-geog-icm`](//bitbucket.org/bryankaraffa/ucsb-geog-icm/) where all the documentation and various modules live.

## Why was it developed?
This project was developed for and currently in use as the official building directory in [Ellison Hall](http://link.map.geog.ucsb.edu/b8), the home of the [UCSB Geography Department](http://geog.ucsb.edu/).  The original concept was developed by the CNSI department (Thanks Paul!).  It was developed out of necessity, however, using the UCSB Interactive Campus Map -- another project run by the UCSB Geography Department -- made it extremely easy to develop.

## Recommended/Minimal Requirements
Server Side (to host the map):

 - Web Server with PHP 5.4+ installed
 
Client Side (to access/use the map):

 - 1920 x 1080 resolution Touchscreen Computer/Monitor
 - Web Browser (Chrome in Kiosk mode is what we use in Ellison Hall)
  
  *If you don't have a touchscreen computer or monitor available*, you can still run this using a standard keyboard/mouse.  The biggest concern with this however is preventing your users from traveling away from the directory webpage.  There are many "Kiosk" and solutions to this issue, however that is not something that we cover in these docs or recommend.
  


# Credits & Thank You's

 - BIG Thanks to **Paul Weakliem** for sending us their original concept for the touch-screen directory.  Their touchscreen directory system they have in place at Eling Hall is the inspiration and original "mold" this application was based off of.

 - [Adminer](//www.adminer.org/) -- OpenSource, single file alternative to phpMyAdmin.  Used as to manage and update the people and facilities records.

 - [Bootstrap](//getbootstrap.com/) + [JQuery](//jquery.com/) -- Makes everything look nice and a very powerful combo for building webapps.

 - [Fat Free Framework](//fatfreeframework.com/) -- From their website, and I definitely agree and will use it in the future:  "A powerful yet easy-to-use PHP micro-framework design to help you build dynamic and robust web applications - fast!"
 
 - Another shoutout for the [UCSB Geography Department](http://geog.ucsb.edu/) for their outstanding work on the [UCSB Interactive Campus Map](http://map.geog.ucsb.edu/) project.  Without the UCSB ICM, it would be very difficult to display the locate and display locations on campus.